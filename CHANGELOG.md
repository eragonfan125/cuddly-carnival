# WallWorlds Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## 0.1.0 - 2015-03-12
### Added
- INSTALL.md, for instructions on installing
- README.md, for instructions on playing WallWorlds
- For the command syntax of Wallworlds, read